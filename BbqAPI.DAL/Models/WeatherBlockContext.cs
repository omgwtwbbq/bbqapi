﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

    namespace BbqAPI.DAL.Models
    {
        public class WeatherBlockContext : DbContext
        {
            public WeatherBlockContext(DbContextOptions<WeatherBlockContext> options)
                : base(options)
            {
            }

            public DbSet<WeatherBlock> WeatherBlocks { get; set; }
        }
    }

