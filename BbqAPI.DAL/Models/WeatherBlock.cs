﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BbqAPI.DAL.Models
{
    public class WeatherBlock
    {
        public int Id { get; set; }
        public int? CloudCoverPercent { get; set; }
        public DateTime? DateTimeUnix { get; set; }
        public string DateTimeTxt { get; set; }
        public double? TempActualKelvin { get; set; }
        //public float? TempFeelsLikeKelvin { get; set; }
        //public int? VisibilityDistanceMeters { get; set; }
        //public float? WindSpeedMetersPerSecond { get; set; }
        //public int? HumidityPercent { get; set; }
        //public int? PressureMillibars { get; set; }
        //public int? ProbabilityOfPrecipitationPercent { get; set; }
    }
}

