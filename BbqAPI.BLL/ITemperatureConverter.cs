﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BbqAPI.BLL
{
    public interface ITemperatureConverter
    {
        int ConvertKelvinToFahrenheit(int temperatureInKelvin);

    }
}
