﻿using System;
using BbqAPI.DAL.Models;

namespace BbqAPI.BLL
{
    public class TemperatureConverter : ITemperatureConverter
    {
        public int ConvertKelvinToFahrenheit(int temperatureInKelvin)
        {
            double kelvinFactor = 1.8;
            double temperatureInFahrenheit = temperatureInKelvin*kelvinFactor-459.67;
            return (int) Math.Ceiling(temperatureInFahrenheit);
        }
    }
}
