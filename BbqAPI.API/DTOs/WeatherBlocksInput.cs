﻿using System;

namespace BbqAPI.API.DTOs
{
    public class WeatherBlocksInput
    {
        public int CloudCoverPercent { get; set; }
        public string DateTimeTxt { get; set; }
        public float TempActualKelvin { get; set; }
        //public float? TempFeelsLikeKelvin { get; set; }
        //public int? VisibilityDistanceMeters { get; set; }
        //public float? WindSpeedMetersPerSecond { get; set; }
        //public int? HumidityPercent { get; set; }
        //public int? PressureMillibars { get; set; }
        //public int? ProbabilityOfPrecipitationPercent { get; set; }
    }
}