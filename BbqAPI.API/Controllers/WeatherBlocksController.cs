﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BbqAPI.API.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BbqAPI.DAL.Models;

namespace BbqAPI.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherBlocksController : ControllerBase
    {
        private readonly WeatherBlockContext _context;

        public WeatherBlocksController(WeatherBlockContext context)
        {
            _context = context;
        }

        // GET: api/WeatherBlocks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WeatherBlock>>> GetWeatherBlocks()
        {
            return await _context.WeatherBlocks.ToListAsync();
        }

        // GET: api/WeatherBlocks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<WeatherBlock>> GetWeatherBlock(int id)
        {
            var weatherBlock = await _context.WeatherBlocks.FindAsync(id);

            if (weatherBlock == null)
            {
                return NotFound();
            }

            return weatherBlock;
        }

        // PUT: api/WeatherBlocks/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWeatherBlock(int id, WeatherBlock weatherBlock)
        {
            if (id != weatherBlock.Id)
            {
                return BadRequest();
            }

            _context.Entry(weatherBlock).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WeatherBlockExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/WeatherBlocks
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<WeatherBlock>> PostWeatherBlock(WeatherBlocksInput weatherBlocksInput)
        {
            WeatherBlock weatherBlock = new WeatherBlock();
            weatherBlock.CloudCoverPercent = weatherBlocksInput.CloudCoverPercent;
            weatherBlock.DateTimeTxt = weatherBlocksInput.DateTimeTxt;
            weatherBlock.DateTimeUnix = DateTime.Parse(weatherBlocksInput.DateTimeTxt);
            weatherBlock.TempActualKelvin = weatherBlocksInput.TempActualKelvin;

            _context.WeatherBlocks.Add(weatherBlock);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetWeatherBlock), new { id = weatherBlock.Id }, weatherBlock);
        }

        // DELETE: api/WeatherBlocks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<WeatherBlock>> DeleteWeatherBlock(int id)
        {
            var weatherBlock = await _context.WeatherBlocks.FindAsync(id);
            if (weatherBlock == null)
            {
                return NotFound();
            }

            _context.WeatherBlocks.Remove(weatherBlock);
            await _context.SaveChangesAsync();

            return weatherBlock;
        }

        private bool WeatherBlockExists(int id)
        {
            return _context.WeatherBlocks.Any(e => e.Id == id);
        }
    }
}
