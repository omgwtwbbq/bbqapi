using BbqAPI.BLL;
using NUnit.Framework;

namespace Test.BbqAPI.BLL
{
    public class Tests
    {
        private TemperatureConverter _temperatureConverter;

        [SetUp]
        public void Setup()
        {
            _temperatureConverter = new TemperatureConverter();
        }

        [Test]
        public void KelvinToFahrenheit_WhenGiven255_Returns0()
        {
            //Assemble
            int input = 255;
            int expected = 0;

            //Act
            int actual = _temperatureConverter.ConvertKelvinToFahrenheit(input);

            //Assert
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void KelvinToFahrenheit_WhenGiven294_Returns70()
        {
            //Assemble
            int input = 294;
            int expected = 70;

            //Act
            int actual = _temperatureConverter.ConvertKelvinToFahrenheit(input);

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}